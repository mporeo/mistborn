#!/bin/bash

# Docker
figlet "Mistborn: Installing Docker"

if [ "$DISTRO" == "ubuntu" ] || [ "$DISTRO" == "debian" ] || [ "$DISTRO" == "raspbian" ]; then
    sudo apt-get update
    sudo apt-get install -y python python3-pip python3-setuptools libffi-dev python3-dev libssl-dev
elif [ "$DISTRO" == "arch" ]; then
    sudo pacman -Syyy
    sudo pacman -S --noconfirm python python-pip python-setuptools libffi openssl
fi


if [ "$DISTRO" == "ubuntu" ] && [ "$VERSION_ID" == "20.04" ]; then
    echo "Automated Docker install"
    sudo apt-get install -y docker-compose
elif [ "$DISTRO" == "arch" ]; then
    echo "Semi-Automated Docker install"
    sudo pacman -S --noconfirm docker-compose
    source ./scripts/subinstallers/docker_manual.sh
else
    echo "Manual Docker installation"
    source ./scripts/subinstallers/docker_manual.sh
fi

# set docker-compose path used in Mistborn
if [ ! -f /usr/local/bin/docker-compose ]; then
    sudo ln -s $(which docker-compose) /usr/local/bin/docker-compose
fi
